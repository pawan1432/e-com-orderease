class AppRoutes{
  static const String splash="/splash";
  static const String signIn="/signIn";
  static const String signUp="/signUp";
  static const String forgotPassword="/forgotPassword";
  static const String forgotPasswordMethod="/forgotPasswordMethod";
  static const String home="/home";
}