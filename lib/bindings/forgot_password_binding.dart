import 'package:e_com_order_ease/controllers/forgot_password_controller.dart';
import 'package:get/get.dart';

class ForgotPasswordBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(ForgotPasswordController());
  }
}