import 'package:e_com_order_ease/controllers/sign_in_controller.dart';
import 'package:e_com_order_ease/utils/localisations/app_colors.dart';
import 'package:e_com_order_ease/utils/localisations/image_paths.dart';
import 'package:e_com_order_ease/utils/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

class SignInUI extends StatelessWidget {
  const SignInUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SignInController controller =Get.find();
    return 
      SafeArea(child: Scaffold(
          backgroundColor: AppColors.white,
          body:
          SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child:  Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: 256,
                    width: Get.width,
                    decoration: BoxDecoration(
                        color: AppColors.sky,
                        borderRadius: const BorderRadius.only(
                            bottomRight: Radius.circular(25),
                            bottomLeft: Radius.circular(25))),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap:(){
                            Get.toNamed(AppRoutes.signUp);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              "SIGN UP".text.size(14).color(AppColors.white).make(),

                            ],
                          ).pOnly(right : 31,top: 40),),
                        "Sign in".text.size(30).color(AppColors.white).make().pOnly(top: 30,bottom: 10),
                        SizedBox(
                          width: Get.width*.8,
                          child:  "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Integer maximus accumsan erat id facilisis.".text.size(12).color(AppColors.white).lineHeight(2).make().pOnly(right: 0)
                          ,
                        )
                      ],
                    ).pOnly(left: 20),
                  ),
                  Form(
                      key: controller.formKey,
                      child: Column(

                        children: [
                          TextFormField(
                            controller: controller.emailController,
                            keyboardType: TextInputType.emailAddress,
                            validator: (value){
                              return controller.emailValidator();
                            },
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: AppColors.backgroundColor,
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                hintText: "Enter your Email",
                                hintStyle: TextStyle(
                                    color: AppColors.textGrey)),
                          ).pOnly(bottom: 40),
                          const SizedBox(height: 20,),
                          const PasswordField()

                        ],)).pOnly(left: 12,right: 21,top: 37),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      "Forget password?".text.size(12).color(AppColors.red).make().pOnly(right: 31),
                    ],
                  ).onTap(() {
                    controller.onForgotPassword(context);
                  }),
                  Container(
                    alignment: Alignment.center,
                    height: 62,
                    width: Get.width*.9,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: AppColors.buttonColorBlue
                    ),
                    child: "SIGN IN".text.color(AppColors.white).size(14).make(),
                  ).pOnly(top: 30).onTap(() { controller.signInButtonValidation();}),
                  "Or Sign in with social media".text.size(12).fontWeight(FontWeight.w400).lineHeight(5).make().pOnly(bottom: 25),
                  Container(
                      alignment: Alignment.center,
                      height: 62,
                      width: Get.width*.9,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: AppColors.backgroundColor
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image.asset(ImagesPaths.ic_googleLogo),
                          "CONTINUE WITH GOOGLE".text.size(14).color(AppColors.textGrey).fontWeight(FontWeight.w700).make(),
                        ],
                      )
                  ),
                  const SizedBox(
                    height: 38,
                  ),
                  Container(
                      alignment: Alignment.center,
                      height: 62,
                      width: Get.width*.9,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: AppColors.blue
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image.asset(ImagesPaths.ic_facebookLogo,height: 50,width: 50,),
                          "CONTINUE WITH FACEBOOK".text.size(14).color(AppColors.white).fontWeight(FontWeight.w700).make(),
                        ],
                      )).pOnly(bottom: 40)
                ]
            ),

          )
      ));
  }
}
class PasswordField extends StatefulWidget {
  const PasswordField({Key? key}) : super(key: key);

  @override
  State<PasswordField> createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  @override
  Widget build(BuildContext context) {
    SignInController controller =Get.find();
    return TextFormField(

      validator: (value){
        return controller.passwordValidator();
      },
      controller: controller.passwordController,
      keyboardType: TextInputType.visiblePassword,
      obscureText: controller.isPasswordVisible.value,
      decoration: InputDecoration(
          filled: true,

          fillColor: AppColors.backgroundColor,

          suffixIcon:InkWell(
            onTap:(){
              setState(() {

              });
              controller.showPassword();
            },
            child: Image.asset(ImagesPaths.ic_eye),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(30.0),
          ),
          hintText: "Password",
          hintStyle: TextStyle(
              color: AppColors.textGrey)),
    ).pOnly(bottom: 20);
  }
}

