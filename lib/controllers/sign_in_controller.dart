import 'package:e_com_order_ease/utils/localisations/app_colors.dart';
import 'package:e_com_order_ease/utils/localisations/print_data.dart';
import 'package:e_com_order_ease/utils/routes/app_routes.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/routes/app_pages.dart';

class SignInController extends SuperController {
  ForgotMethods? methods ;
  final formKey = GlobalKey<FormState>();
  RxBool isPasswordVisible = true.obs;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  void chooseForgotMethods({required ForgotMethods type}){
    methods = type;
    Get.toNamed(AppRoutes.forgotPasswordMethod);
  }
void onForgotPassword(context){
  showDialog(context: context, builder: (BuildContext context){
    return
      AlertDialog(
      title: "Forgot Password".text.make().centered(),
      content:     Container(
        height: 100.0,
        width: 200.0,
        child:
       Column(
         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
         Container(
           alignment: Alignment.center,
           height: 40,
           width: Get.width,
           // color: AppColors.sky,
           child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: const [
               Text("Via mail"),
               Icon(Icons.navigate_next_outlined),
             ],
           )
         ).onTap(() {
           Navigator.of(context).pop();
           chooseForgotMethods(type: ForgotMethods.viaMail);

         }),
          Container(
            alignment: Alignment.center,

            height: 40,
           width: Get.width,
          decoration: const BoxDecoration(
            // color:Colors.grey.shade500,
          ),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,

             children: const [
               Text("Via phone"),
               Icon(Icons.navigate_next_outlined),
             ],
           ),
         ).onTap(() {
            Navigator.of(context).pop();
           chooseForgotMethods(type: ForgotMethods.viaPhone);
         }),

        ],

    )),
        actions: <Widget>[
          TextButton(
            child: const Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],

      );
  }
  );
}
  void showPassword() {
    isPasswordVisible.value = !isPasswordVisible.value;
    print(isPasswordVisible);
  }

  String? emailValidator() {
    if (EmailValidator.validate(emailController.text)) {
      return null;
    } else {
      return "Invalid Email";
    }
  }

  String? passwordValidator() {
    if (passwordController.text.length < 8) {
      return "Password must be 8 digits";
    }
  }

  void signInButtonValidation() {
    if (formKey.currentState!.validate()) {
      print("success");
    } else {
      print("failed");
    }
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}
enum  ForgotMethods{viaMail, viaPhone}
