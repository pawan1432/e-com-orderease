import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../controllers/sign_up_controller.dart';
import '../utils/localisations/app_colors.dart';
import '../utils/localisations/image_paths.dart';
import '../utils/routes/app_routes.dart';

class SignUpUI extends StatelessWidget {
  const SignUpUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SignUpController controller = Get.find();
    return 
      SafeArea(child: Scaffold(
          backgroundColor: AppColors.white,
          body: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(children: [
              Container(
                alignment: Alignment.center,
                height: 256,
                width: Get.width,
                decoration: BoxDecoration(
                    color: AppColors.sky,
                    borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(25),
                        bottomLeft: Radius.circular(25))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap:(){
                        Get.toNamed(AppRoutes.signIn);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          "SIGN IN".text.size(14).color(AppColors.white).make(),

                        ],
                      ).pOnly(right : 31,top: 40),),
                    "Sign up"
                        .text
                        .size(30)
                        .color(AppColors.white)
                        .make()
                        .pOnly(top: 30, bottom: 10),
                    SizedBox(
                      width: Get.width * .8,
                      child:
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Integer maximus accumsan erat id facilisis."
                          .text
                          .size(12)
                          .color(AppColors.white)
                          .lineHeight(2)
                          .make()
                          .pOnly(right: 0),
                    )
                  ],
                ).pOnly(left: 20),
              ),
              Form(
                  key: controller.formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: controller.nameController,
                        keyboardType: TextInputType.name,
                        validator: (value) {
                          return controller.nameValidator();
                        },
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: AppColors.backgroundColor,
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            hintText: "Your name",
                            hintStyle: TextStyle(color: AppColors.textGrey)),
                      ).pOnly(bottom: 20),
                      TextFormField(
                        controller: controller.emailController,
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          return controller.emailValidator();
                        },
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: AppColors.backgroundColor,
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            hintText: "Your Email",
                            hintStyle: TextStyle(color: AppColors.textGrey)),
                      ).pOnly(bottom: 20),

                      // const PasswordField(),
                      Obx(() => TextFormField(
                        validator: (value) {
                          return controller.passwordValidator();
                        },
                        controller: controller.passwordController,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: controller.isPasswordVisible.value,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: AppColors.backgroundColor,
                            suffixIcon: InkWell(
                              onTap: () {
                                // setState(() {
                                //
                                // });
                                controller.showPassword();
                              },
                              child: Image.asset(ImagesPaths.ic_eye),
                            ),
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            hintText: " Your password",
                            hintStyle: TextStyle(color: AppColors.textGrey)),
                      )).pOnly(bottom: 20),
                      Obx(() => TextFormField(
                        validator: (value) {
                          return controller.confirmPasswordValidator();
                        },
                        controller: controller.confirmPasswordController,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: controller.isPasswordVisible.value,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: AppColors.backgroundColor,
                            suffixIcon: InkWell(
                              onTap: () {
                                controller.showPassword();
                              },
                              child: Image.asset(ImagesPaths.ic_eye),
                            ),
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            hintText: "Confirm  password",
                            hintStyle: TextStyle(color: AppColors.textGrey)),
                      )).pOnly(bottom: 20)
                      // const PasswordField(),
                    ],
                  )).pOnly(left: 12, right: 21, top: 37),
              Container(
                alignment: Alignment.center,
                height: 62,
                width: Get.width * .9,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: AppColors.buttonColorBlue),
                child: "SIGN UP".text.color(AppColors.white).size(14).make(),
              ).pOnly(top: 30).onTap(() {
                controller.signUpButtonValidation();
              }),
              "Or Sign in with social media"
                  .text
                  .size(12)
                  .fontWeight(FontWeight.w400)
                  .lineHeight(5)
                  .make()
                  .pOnly(bottom: 25),
              Container(
                  alignment: Alignment.center,
                  height: 62,
                  width: Get.width * .9,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: AppColors.backgroundColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Image.asset(ImagesPaths.ic_googleLogo),
                      "CONTINUE WITH GOOGLE"
                          .text
                          .size(14)
                          .color(AppColors.textGrey)
                          .fontWeight(FontWeight.w700)
                          .make(),
                    ],
                  )),
              const SizedBox(
                height: 38,
              ),
              Container(
                  alignment: Alignment.center,
                  height: 62,
                  width: Get.width * .9,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: AppColors.blue),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Image.asset(
                        ImagesPaths.ic_facebookLogo,
                        height: 50,
                        width: 50,
                      ),
                      "CONTINUE WITH FACEBOOK"
                          .text
                          .size(14)
                          .color(AppColors.white)
                          .fontWeight(FontWeight.w700)
                          .make(),
                    ],
                  )).pOnly(bottom: 40)
            ]),
          )));
  }
}
