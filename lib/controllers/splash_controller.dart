import 'package:e_com_order_ease/utils/routes/app_routes.dart';
import 'package:get/get.dart';

class SplashController extends SuperController{
@override
  void onInit() {
    Future.delayed(Duration(seconds: 5),(){
      Get.offAllNamed(AppRoutes.signIn);
      print("splash done");
    });
    super.onInit();
  }
  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}