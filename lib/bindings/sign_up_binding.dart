import 'package:e_com_order_ease/controllers/sign_in_controller.dart';
import 'package:get/get.dart';

import '../controllers/sign_up_controller.dart';

class SignUpBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(SignUpController());
  }
}