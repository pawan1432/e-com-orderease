// ignore_for_file: non_constant_identifier_names, unnecessary_null_comparison

import 'package:e_com_order_ease/utils/localisations/app_colors.dart';
import 'package:e_com_order_ease/utils/localisations/print_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../utils/routes/app_pages.dart';
import '../utils/routes/app_routes.dart';

class ForgotPasswordController extends SuperController{
  final formkey=GlobalKey<FormState>();
  var firstFocusNode=FocusNode();
  final secondFocusNode=FocusNode();
  final thirdFocusNode=FocusNode();
  final fourthFocusNode=FocusNode();
  TextEditingController firstFieldController=TextEditingController();
  TextEditingController secondFieldController=TextEditingController();
  TextEditingController thirdFieldController=TextEditingController();
  TextEditingController fourthFieldController=TextEditingController();

  // ignore: body_might_complete_normally_nullable
  void deleteOTP({required FieldType fieldType}) {
    if (fieldType == FieldType.first) {
      if (firstFieldController.text.isNotEmpty) {

        firstFocusNode.unfocus();
        secondFocusNode.requestFocus();
      }
    } else if (fieldType == FieldType.second) {
      if (secondFieldController.text.isNotEmpty) {

        secondFocusNode.unfocus();
        thirdFocusNode.requestFocus();
      } else {


        secondFocusNode.unfocus();
        firstFocusNode.requestFocus();
      }
    } else if (fieldType == FieldType.third) {
      if (thirdFieldController.text.isNotEmpty) {

        thirdFocusNode.unfocus();
        fourthFocusNode.requestFocus();
      } else {


        thirdFocusNode.unfocus();
        secondFocusNode.requestFocus();
      }
    } else if (fieldType == FieldType.fourth) {
      if (fourthFieldController.text.isNotEmpty) {

        fourthFocusNode.unfocus();
      }else {


        fourthFocusNode.unfocus();
        thirdFocusNode.requestFocus();
      }
    }
  }


  void onConfirmButtonSubmitted(){

    if(firstFieldController.text==""||secondFieldController.text==""||thirdFieldController.text==""||fourthFieldController.text==""){
       GetSnackBar(
        borderRadius:10 ,
        borderColor: AppColors.sky,
       
        titleText:"OrderEase".text.color(AppColors.white).size(20).fontWeight(FontWeight.bold).make() ,
        snackStyle: SnackStyle.FLOATING,
        messageText: "Please fill all the fields".text.color(AppColors.white).size(14).make(),
        backgroundColor:AppColors.fadedBlack,
        duration: Duration(seconds: 3),
        snackPosition: SnackPosition.TOP,
      ).show();
    }
    else if(firstFieldController.text+secondFieldController.text+thirdFieldController.text+fourthFieldController.text!="1234") {
      GetSnackBar(
        borderRadius:10 ,
        borderColor: AppColors.sky,

        titleText:"OrderEase".text.color(AppColors.white).size(20).fontWeight(FontWeight.bold).make() ,
        snackStyle: SnackStyle.FLOATING,
        messageText: "OTP doesn't match".text.color(AppColors.white).size(14).make(),
        backgroundColor:AppColors.fadedBlack,
        duration: Duration(seconds: 3),
        snackPosition: SnackPosition.TOP,
      ).show();

    }
    else{
      // Get.offAllNamed(AppRoutes.signUp);
      printData("moving to sign up screen");
    }
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}
enum FieldType { first, second, third, fourth }