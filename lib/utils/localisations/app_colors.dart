import 'package:flutter/material.dart';

class AppColors {
  static Color fadedBlack = const Color(0xFF57636F);
  static Color white = const Color(0xFFFFFFFF);
  static Color fadedBlue = const Color(0xFF126881);
  static Color sky = const Color(0xFF7BCFE9);
  static Color yellow = const Color(0xFFFBBA32);
  static Color orange = const Color(0xFFFFA771);
  static Color pink = const Color(0xFFE4126B);
  static Color red = const Color(0xFFE41A4A);
  static Color grey = const Color(0xFF7A8D9C);
  static Color textGrey = const Color(0xFF57636F);
  static Color blue = const Color(0xFF1877F2);

  static Color buttonColorBlue = const Color(0xFF126881);
  static Color buttonColorGrey = const Color(0xFFACBAC3);
  static Color backgroundColor = const Color(0xFFF6F6F7);
  static Color scaffoldBackground = const Color(0xFF4A4A4A);


}