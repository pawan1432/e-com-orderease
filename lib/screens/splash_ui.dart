import 'package:e_com_order_ease/utils/localisations/app_colors.dart';
import 'package:e_com_order_ease/utils/localisations/image_paths.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class SplashUI extends StatelessWidget {
  const SplashUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
    body: Center(
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
       Lottie.asset(ImagesPaths.splash,fit: BoxFit.cover),
        ],
      )
    ),
    );
  }
}
