import 'package:e_com_order_ease/bindings/forgot_password_binding.dart';
import 'package:e_com_order_ease/bindings/forgot_password_method_binding.dart';
import 'package:e_com_order_ease/bindings/home_binding.dart';
import 'package:e_com_order_ease/bindings/sign_in_binding.dart';
import 'package:e_com_order_ease/screens/forgot_password_method_ui.dart';
import 'package:e_com_order_ease/screens/forgot_password_ui.dart';
import 'package:get/get.dart';

import '../../bindings/sign_up_binding.dart';
import '../../bindings/splash_binding.dart';
import '../../screens/home_ui.dart';
import '../../screens/sign_in_ui.dart';
import '../../screens/sign_up_ui.dart';
import '../../screens/splash_ui.dart';
import 'app_routes.dart';

class AppPages {
  static const initialRoute = AppRoutes.signIn;
  static final route = [
    GetPage(
        name: AppRoutes.splash,
        page: () => const SplashUI(),
        binding: SplashBinding()),
    GetPage(
        name: AppRoutes.signIn,
        page: () => const SignInUI(),
        binding: SignInBinding()),
    GetPage(
        name: AppRoutes.signUp,
        page: () => const SignUpUI(),
        binding: SignUpBinding()),
    GetPage(
        name: AppRoutes.forgotPassword,
        page: () => const ForgotPasswordUI(),
        binding: ForgotPasswordBinding()),
    GetPage(
        name: AppRoutes.forgotPasswordMethod,
        page: () => const ForgotMethodUI(),
        binding: ForgotMethodBinding()),
    GetPage(
        name: AppRoutes.home,
        page: () => const HomeUI(),
        binding: HomeBinding()),
  ];
}
