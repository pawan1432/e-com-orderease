import 'package:e_com_order_ease/controllers/forgot_password_method_controller.dart';
import 'package:get/get.dart';

class ForgotMethodBinding implements Bindings{
  @override
  void dependencies() {
    Get.put(ForgotMethodController());
  }
}