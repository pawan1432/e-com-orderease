import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class HomeUI extends StatelessWidget {
  const HomeUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: "W E L C O M E".text.make(),
      ),
    );
  }
}
