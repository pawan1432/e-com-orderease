// ignore_for_file: body_might_complete_normally_nullable

import 'package:e_com_order_ease/utils/routes/app_routes.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignUpController extends SuperController{
  final formKey = GlobalKey<FormState>();
  RxBool isPasswordVisible = true.obs;
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  void showPassword() {
    isPasswordVisible.value = !isPasswordVisible.value;
    print(isPasswordVisible);
  }

  String? emailValidator() {
    if (EmailValidator.validate(emailController.text)) {
      return null;
    } else {
      return "Invalid email";
      // kjk
    }
  }

String? nameValidator() {
    if (nameController.text.length<3) {
      return "Invalid name";
    }
  }

  String? passwordValidator() {
    if (passwordController.text.length < 8) {
      return "Password must be 8 digits";
    }
  }
  String? confirmPasswordValidator() {
    if (passwordController.text!=confirmPasswordController.text) {
      return "New password does not match";
    }
  }

  void signUpButtonValidation() {
    if (formKey.currentState!.validate()) {
      Get.offAllNamed(AppRoutes.home);
    } else {
      print("failed");
    }
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}